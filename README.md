# PHP image Apache

## Introduction
This image consists of PHP 7.2 and Apache HTTP server to server PHP powered sites
directly from port 80 in the browser. The apt package ssmtp is added to emailing in
PHP can be used and configured using the ENV variables mentioned below.

The following extensions are installed:

- `MySQL`: for access to MySQL databases
- `Memcached`: for data cache
- `ODBC`: for unit testing
- `XDebug`: for inline debugging

Also installed is `PHPUnit` to run unit tests.


## Configure using environment variables

The following environment variables can be set:

- `PHP_MAIL_IP: 'mailcatcher'`: IP or hostname of the mailserver
- `PHP_MAIL_PORT: '25'`: Port of the mailserver
- `PHP_MAIL_USERNAME: ''`: If required, the username for access to the mailserver
- `PHP_MAIL_PASSWORD: ''`: If required, the password for access to the mailserver
- `PHP_MAIL_USE_TLS: 'no'`: Use TLS for connection to mailserver
- `PHP_XDEBUG_ENABLE: 'Off'`: Enable Xdebug On/Off
- `PHP_OPCACHE_ENABLE: 'On'`: Enable Opcache On/Off
- `PHP_ZLIB_OUTPUT_COMPRESSION_ENABLE: 'On'`: Enable Zlib output compression On/Off
- `PHP_ZLIB_OUTPUT_COMPRESSION_LEVEL: '-1'`: Zlib output compression level.
Specify a value between 0 (no compression) to 9 (most compression).
The default value, -1, lets the server decide which level to use.
- `PHP_TIMEZONE: 'Europe/Amsterdam'`: Set the timezone for the shell and also PHP

## Memcached

This image has Memcached enabled by default. For information how to use Memcached
in your project, take a look at this [Memcached with PHP example](https://github.com/apache/ignite/blob/master/examples/memcached/memcached-example.php).


## XDebug

To better debug your application without needing to use var_dump and exit all
the time, XDebug is a great tool. This setup has XDebug enabled by default
so if your IDE supports it, you can set debug steps from within your code.

When XDebug is enabled (by setting the XDebug Session Cookie) the debug information
is send to port 9001 on your pc. So it's not nescasary to expose port 9001 in your
container.

In the documentation, we only show the highlights of how to setup and use XDebug.
To get you started with more detail, have a look at
[Debugging PHP applications with xdebug](https://devzone.zend.com/1147/debugging-php-applications-with-xdebug/).

### Setup Sublime Text 3 for XDebug
This example uses Sublime as an editor, but there are many online examples for
other populair IDE's. You should read this great tutorial about
[XDebug and Sublime Text](http://www.vaerenbergh.com/blog/xdebug-and-sublime-text)
to install the plugin and know how to use it.

For Sublime Text 3 to be able to show XDebug info, you can install the following
plugin: [SublimeTextXdebug](https://github.com/martomo/SublimeTextXdebug).

After installing the plugin, you need to modify the project settings for
the project(s) for which you want to enable XDebug. You can do this by editing
the project settings navigation to the menu `Project > Edit project` within
Subline Text 3. Make sure it has the following content:

```json
	"settings": {
        "xdebug": {
        	"url": "http://ansulcrm.local",
        	"port": 9001,
            "ide_key": "sublime.xdebug"
       	}
    }
```

### How to use it?
To turn debugging on goto `Tools > XDebug > Start debugging (Launch browser)`. From the moment you turn
on debugging every page you visit in the browser will be debugged. So if there are errors or warnings
within the source code that is used to parse that page, it will halt in the browser and show you in your
IDE (Sublime) what the problem is. This will be made visible in the right lower screen. The left lower
screen will show you the dump of the variables. If you have object or struct variables, you can double
click on the yellow dots behind the variable name to see the values.

#### Breakpoints
It will stop the execution on set breakpoints, but also on warnings in the code.
Sublime will display the file and mark the line on the ruler with a circle (break point)
or a warning/error (yellow arrow).

#### Watch xdebug logs
To troubleshoot xdebug errors within your container, you can see the log that's being generated
in hopes to find the error soon. Run the following command at the cmd-prompt:

```bash
docker exec -it ansulcrm tail -f /tmp/xdebug.log
```
**NOTE:** `ansulcrm` is the name of the container and should be replaced by your container name.

#### Stop XDebug
Stopping XDebug is really easy. Goto `Tools > XDebug > Stop debugging (Launch browser)`.

### Used sources
To find the best way to use XDebug in our Docker setup, several online sources where used:
- [Using Xdebug with Docker and Sublime Text](https://github.com/vhoen/docker-xdebug-sublime-text)
- [Debugging with Xdebug and Sublime Text 3](https://www.sitepoint.com/debugging-xdebug-sublime-text-3/)
- [How to Install Xdebug with PHPStorm and Vagrant](https://www.sitepoint.com/install-xdebug-phpstorm-vagrant/)
- [Debug your PHP in Docker with Intellij/PHPStorm and Xdebug](https://gist.github.com/jehaby/61a89b15571b4bceee2417106e80240d)
- [install Xdebug - Xdebug: Documentation](https://xdebug.org/docs/install)


## PHPUnit

PHPUnit is a programmer-oriented testing framework for PHP.

On the website you will find a brief [getting started](https://phpunit.de/getting-started.html),
but for more detailed information on how to write tests, it's best to take a look at
[Writing Tests for PHPUnit](https://phpunit.de/manual/current/en/writing-tests-for-phpunit.html).

PHPUnit in this image is setup using a bootstrap.php file in the tests directory: `tests/bootstrap.php`.
If this exists, the `command tests` step in the `.gitlab-ci.yml` file of your project
will run the tests automatically.

### Run PHPUnit directly within running container
```bash
docker exec -it ansulcrm /usr/local/bin/phpunit \
	--bootstrap tests/bootstrap.php \
	--whitelist tests \
	--debug \
	--colors=always tests
```
Or in one line:
```bash
docker exec -it ansulcrm /usr/local/bin/phpunit --bootstrap tests/bootstrap.php --whitelist tests --debug --colors=always tests
```

**NOTE:** `ansulcrm` is the name of the container and should be replaced by your container name.


## PHPLint

PHPLint is a tool that will help you write better code by checkin the code almost realtime
when you are typing it. Because we use Sublime Text 3 as an IDE, we will focus on the
installation and usage in this IDE.

### Installation
To be able to use PHPLinter, we need to follow three different installations.

* Download phplint
* Install SublimeLinter
(http://sublimelinter.readthedocs.io/en/latest/)
* Install SublimeLinter-phplint
(https://github.com/SublimeLinter/SublimeLinter-phplint)

### Download phplint
Go to [PHPlint download site](http://www.icosaedro.it/phplint/download.html) and download the file `phplint-3.0_20160307.zip`.
Unzip the fil in your `C:\Program Files (x86)` directory, creating the directory
`C:\Program Files (x86)\phplint-3.0_20160307`. Then add this directory to your path:
Open `Verkenner` and with the right mouse button click on `Deze PC` and follow
`Systeemeigenschappen > Omgevingsvariabelen > Path `. Near `Path` add the path
`C:\Program Files (x86)\phplint-3.0_20160307`.

### Install SublimeLinter and SublimeLinter-phplint
Install `SublimeLinter` and `SublimeLinter-phplint` packages within Sublime 3 by going
to the menu
`Preferences > Package Control > Install Package` and install package `SublimeLinter`
and after the installation is complete install the package `SublimeLinter-phplint`.