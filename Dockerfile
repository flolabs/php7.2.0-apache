FROM php:7.2.0-apache

MAINTAINER flolopdel@gmail.com

# OS variables
ENV DEBIAN_FRONTEND='noninteractive'
ENV TZ='Europe/Madrid'
ENV TERM='xterm'
ENV PHP_ADABAS='no'

# PHP Mail settings
ENV PHP_MAIL_IP='mailcatcher'
ENV PHP_MAIL_PORT='25'
#ENV PHP_MAIL_USERNAME=''
#ENV PHP_MAIL_PASSWORD=''
#ENV PHP_MAIL_USE_SSL='no'
ENV PHP_MAIL_USE_TLS='no'
ENV PHP_XDEBUG_ENABLE='Off'
ENV PHP_XDEBUG_REMOTE_HOST='localhost'
ENV PHP_OPCACHE_ENABLE='On'
ENV PHP_ZLIB_OUTPUT_COMPRESSION_ENABLE='On'
ENV PHP_ZLIB_OUTPUT_COMPRESSION_LEVEL='-1'

RUN apt-get -qq -o=Dpkg::Use-Pty=0 update \
    && apt-get -y -qq -o=Dpkg::Use-Pty=0 install \
        apt-utils \
        rsyslog \
        locales \
        cron \
        git \
        zip \
        nano \
        unzip \
        ssmtp \
        wget \
        unixodbc-dev \
        imagemagick \
        libfreetype6-dev \
        libjpeg-dev \
        libmcrypt-dev \
        #libpng12-dev \
        libmemcached-dev \
        libmagickwand-dev \
        libmagickcore-dev \
        zlib1g-dev \
        libmemcached-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN sed -i 's/^# *\(es_ES.UTF-8\)/\1/' /etc/locale.gen \
    && sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen \
    && sed -i 's/^# *\(cron.*\)/\1/' /etc/rsyslog.conf \
    && service rsyslog start \
    && service cron start \
    && locale-gen

# OS Language variables
ENV LANG='es_ES.UTF-8'
ENV LANGUAGE='es_ES.UTF-8'
ENV LC_ALL='es_ES.UTF-8'

# Install xDebug, if enabled
#ARG INSTALL_XDEBUG=false
RUN if [ ${PHP_XDEBUG_ENABLE} = On ]; then \
    # Install the xdebug extension
    pecl install xdebug-2.5.5 && \
    docker-php-ext-enable xdebug  \
;fi

RUN printf "\n" | pecl install imagick \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-png-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN pecl install mcrypt-1.0.1
RUN docker-php-ext-enable mcrypt
RUN docker-php-ext-install -j$(nproc) zip iconv mysqli sockets gd opcache pdo_mysql

# Install the php memcached extension
RUN curl -L -o /tmp/memcached.tar.gz "https://github.com/php-memcached-dev/php-memcached/archive/php7.tar.gz" \
  && mkdir -p memcached \
  && tar -C memcached -zxvf /tmp/memcached.tar.gz --strip 1 \
  && ( \
    cd memcached \
    && phpize \
    && ./configure \
    && make -j$(nproc) \
    && make install \
  ) \
  && rm -r memcached \
  && rm /tmp/memcached.tar.gz \
  && docker-php-ext-enable memcached

RUN docker-php-ext-enable imagick

# install PHPUnit: https://phpunit.de/
RUN wget -q https://phar.phpunit.de/phpunit-5.7.phar \
    && chmod +x phpunit-5.7.phar \
    && mv phpunit-5.7.phar /usr/local/bin/phpunit

# Enable mod_rewrite for Apache
RUN a2enmod -q rewrite expires headers deflate

# install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install ODBC
RUN docker-php-source extract \
    && set -x \
    && cd /usr/src/php/ext/odbc \
    && phpize \
    && sed -ri 's@^ *test +"\$PHP_.*" *= *"no" *&& *PHP_.*=yes *$@#&@g' configure \
    && docker-php-ext-configure odbc --with-unixODBC=shared,/usr \
    && docker-php-ext-install odbc \
    && docker-php-source delete

# Fix bug: could not reliably determine the server's fully qualified domain name
RUN echo "ServerName 127.0.0.1" | tee -a /etc/apache2/conf-available/server-name.conf \
    && a2enconf server-name

# copy sample phpunit.xml file
COPY .artifacts/phpunit.xml /var/www

# Change Apache logging to not show access loggin
# RUN find /etc/apache2/conf-enabled/* -exec sed -i 's/#*CustomLog/#CustomLog/g' {} \; \
#   && sed -i 's/#*CustomLog/#CustomLog/g' /etc/apache2/apache2.conf \
#   && sed -i 's/#*CustomLog/#CustomLog/g' /etc/apache2/sites-enabled/000-default.conf
# test email
# apt-get -qq -y install mailutils
# echo test | mail -v -s "testing ssmtp setup" flolopdel@gmail.com

# Overwrite entrypoint and add custom config file
COPY build/ /usr/local/bin/
RUN chmod +x /usr/local/bin/set-custom-config.sh